import { IAction } from '@shared/types';
import {
  createReduxOperation,
  actionFlags,
  augmentReducer,
  IReduxOperations
} from '@shared/utils';
import { combineReducers } from 'redux';

const {
  actions: [authRequest, authSuccess, authFailure],
  constants: authConstants,
  reducer: authenticationReducer
} = createReduxOperation('AUTHENTICATE');

export { authSuccess, authRequest, authFailure };

export interface ILoginState {
  metaProp: string;
}

const initialState = {
  metaProp: ''
};

const loginReducer = (state = initialState, action: IAction<Symbol, any>) => {
  console.log(state, action);
  switch (action.type) {
    case authConstants.get(actionFlags.REQUEST):
      return {
        ...state,
        metaProp: 'SET NOW'
      };
    default:
      return state;
  }
};

export interface ILoginStateAugmented extends ILoginState {
  auth: IReduxOperations;
}

export default augmentReducer<ILoginStateAugmented, IAction<Symbol, any>>(
  loginReducer
)({
  auth: authenticationReducer
});
