import React, { useEffect } from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

import './index.scss';
import useRedux from '@shared/redux-hooks';

interface ILoginProps {}

const LoginComponent: React.FC<ILoginProps> = props => {
  const mappedProps = useRedux(s => s, dispatch => {});
  console.log('mappedProps', mappedProps);
  // useEffect(() => {
  //   console.log(props);
  // });
  return (
    <main className="mainHolder">
      <Card className="loginCard">
        <CardContent className="loginCardContent">
          <Typography gutterBottom variant="h5" component="h2">
            Login Form
          </Typography>
          <TextField
            label="Email"
            type="email"
            autoComplete="email"
            margin="normal"
            variant="outlined"
            autoFocus
          />
          <TextField
            label="Password"
            type="password"
            autoComplete="current-password"
            margin="normal"
            variant="outlined"
          />
        </CardContent>
        <CardActions className="cardActions">
          <Button size="large" color="primary" variant="contained">
            Login
          </Button>
        </CardActions>
      </Card>
    </main>
  );
};

export default LoginComponent;
