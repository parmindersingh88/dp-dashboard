import { combineReducers } from 'redux';

import loginReducer from '@src/cep/scenes/Login/login.redux';
import { IRootState } from '@shared/types';

export default combineReducers<IRootState>({
  login: loginReducer
});
