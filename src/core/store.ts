import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './rootReducer';
import rootSaga from './rootSaga';
import logger from './loggerMiddleware';
import { authRequest } from '@src/cep/scenes/Login/login.redux';

export default (preloadedState?: any) => {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [logger<any>(), sagaMiddleware];
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const composedEnhancers = composeWithDevTools({ serialize: true })(
    middlewareEnhancer
  );

  const store = createStore(rootReducer, preloadedState, composedEnhancers);

  sagaMiddleware.run(rootSaga);
  store.dispatch(authRequest());
  return store;
};
