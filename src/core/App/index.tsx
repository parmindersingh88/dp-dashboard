import React, { Component } from 'react';
import { createStoreContext } from '@src/shared/redux-hooks';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import cyan from '@material-ui/core/colors/cyan';

import LoginComponent from '@src/cep/scenes/Login';
import './index.scss';
import createStore from '../store';

/* https://material-ui.com/style/typography/#migration-to-typography-v2 */
window.__MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__ = true;

const theme = createMuiTheme({
  palette: {
    primary: {
      light: cyan[300],
      main: cyan[500],
      dark: cyan[700],
      contrastText: '#fff'
    },
    type: 'light'
  }
});
const store = createStore();

const { Provider } = createStoreContext<any>();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider theme={theme}>
          <Router>
            <Route path="/login" component={LoginComponent} />
          </Router>
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default App;
