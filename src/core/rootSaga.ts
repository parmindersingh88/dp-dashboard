import { all, takeEvery, call } from 'redux-saga/effects';

function* testSaga() {
  yield call(console.log, 'Here');
}

export default function* rootSaga() {
  yield all([takeEvery('TEST_SAGA', testSaga)]);
}
