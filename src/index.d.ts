declare interface IObject<T> {
  [key: string]: T;
}

declare interface Window {
  __MUI_USE_NEXT_TYPOGRAPHY_VARIANTS__: boolean;
  __REDUX_DEVTOOLS_EXTENSION__: any;
}
