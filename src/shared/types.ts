export interface IKeyValueType {
  value: string;
  displayName: string;
  hintText?: string;
}

export interface IAction<T extends Symbol, P = any> {
  type: T;
  payload?: P;
}

export type IActionCreator<T extends Symbol, P = any> = (
  type: T
) => (payload?: P) => IAction<T, P>;

export interface IRootState {
  login: any;
}
