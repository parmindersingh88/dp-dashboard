import { useEffect, useContext, useState, createContext, Context } from 'react';
import { Dispatch, Action, Store } from 'redux';

import ProviderComponent from './Provider';

export type MapStateToProps<State, TStateProps, TOwnProps> = (
  state: State,
  ownProps?: TOwnProps
) => TStateProps;

export type MapDispatchToProps<TDispatchProps, TOwnProps> = (
  dispatch: Dispatch<Action>,
  ownProps?: TOwnProps
) => TDispatchProps;

let context: Context<Store>;

const createStoreContext = <RootState>() => {
  context = createContext<Store<RootState>>(null as any);
  const Provider = ProviderComponent(context);
  return {
    context,
    Provider
  };
};

const useRedux = <
  State = {},
  TStateProps = {},
  TDispatchProps = {},
  TOwnProps = {}
>(
  mapStateToProps: MapStateToProps<State, TStateProps, TOwnProps>,
  mapDispatchToProps: MapDispatchToProps<TDispatchProps, TOwnProps>,
  ownProps?: TOwnProps
) => {
  const { getState, dispatch, subscribe } = <any>useContext(context);
  const state = getState();
  const mappedState = mapStateToProps(state, ownProps);
  const mappedDispatchProps = mapDispatchToProps(dispatch, ownProps);
  const [, setMappedState] = useState(mappedState);
  useEffect(() =>
    subscribe(() => {
      const nextState = getState();
      if (nextState !== state) {
        setMappedState(nextState);
      }
    })
  );
  return {
    ...mappedState,
    ...mappedDispatchProps
  };
};

export { createStoreContext };
export default useRedux;
