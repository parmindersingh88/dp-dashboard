import { IAction } from '@shared/types';

export type IReducer<S, A extends IAction<Symbol, any>> = (
  state: S,
  action: A
) => S;

export const augmentReducer = <S, A extends IAction<Symbol, any>>(
  rootReducer: IReducer<any, A>
) => (reducers: { [k: string]: IReducer<any, A> }) => {
  return (state: S, action: A) => {
    const nextState = {};
    console.log('state', state, action.type);
    const nextRootState = rootReducer(state, action);
    Object.assign(nextState, nextRootState);
    for (const [key, reducer] of Object.entries(reducers)) {
      if (state) {
        const nextStateForKey = reducer(state[key], action);
        nextState[key] = nextStateForKey;
      }
    }
    return nextState;
  };
};
