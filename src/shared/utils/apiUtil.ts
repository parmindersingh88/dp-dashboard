import { ObjectToMap } from '.';

const COMMON_HEADERS = {
  'X-CUSTOM-HEADER': 'abc'
};

export interface IRequestOptions {
  queryParams?: Map<string, string | number | boolean>;
  headers?: Map<string, string>;
  body?: any;
}

export default class ApiUtil {
  get commonHeaders() {
    return ObjectToMap(COMMON_HEADERS);
  }

  request(requestInfo: any, payload?: any) {
    const { headers } = requestInfo;
    const actualHeaders = new Headers();
    // If headers were passed as a Map to the request, iterate and append it to actualHeaders
    if (headers) {
      for (const [key, value] of headers) {
        actualHeaders.append(key, value);
      }
    }
    // Append common headers to actualHeaders
    for (const [key, value] of this.commonHeaders) {
      actualHeaders.append(key, value);
    }
    requestInfo.headers = actualHeaders;
    requestInfo.body = payload;
    return fetch(requestInfo);
  }

  formQueryString(queryParams?: Map<string, string | number | boolean>) {
    if (queryParams) {
      const queryParamsArr: Array<string> = [];
      for (const [key, value] of queryParams) {
        queryParamsArr.push(`${key}=${value}`);
      }
      return queryParamsArr.join('&');
    }
  }

  get(url: string, { queryParams, ...options }: IRequestOptions) {
    const queryString = this.formQueryString(queryParams);
    return this.request({
      method: 'GET',
      url: queryString ? `${url}?${queryString}` : url,
      ...options
    });
  }

  post(url, options: IRequestOptions) {
    return this.request({
      method: 'POST',
      url
    });
  }
}
