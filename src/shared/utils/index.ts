import { IAction, IActionCreator } from '../types';

export { default as apiUtil } from './apiUtil';
export * from './makeConstants';
export * from './createReduxOperation';
export * from './augmentReducer';

export const get = () => {};

export const ObjectToMap = (object: IObject<string>) => {
  return new Map(Object.entries(object));
};

export const createAction: IActionCreator<Symbol> = (
  type: Symbol
) => (): IAction<Symbol> => ({
  type
});

export const createActionWithPayload: IActionCreator<Symbol, any> = <
  T extends Symbol,
  P
>(
  type: T
) => (payload?: P): IAction<T, P> => ({
  type,
  payload
});
